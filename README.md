# Project Title
Robot walk on 5*5

## Getting Started
### Setting up
Follow these steps to run the project:

On console:
```npm install```

Make sure you have mocha and typescript installed globally. 

### Running the program
Step1: run ```tsc``` on console to have all typescript files compiled to JS into /dist directory.

Step2: run ```npm start``` to start the program

### Running test cases
On console:
```npm test```

Note: A few complex test cases have been provided in the test files. 



