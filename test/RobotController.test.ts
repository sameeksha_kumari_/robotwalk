import { expect } from 'chai';

import { RobotController } from '../src/RobotController';
import { Robot } from '../src/Robot';
import { CommandSplitter } from '../src/CommandSplitter';
import { CommandValidator } from '../src/CommandValidator';
import { describe } from 'mocha';

import { 
        ERROR_EMPTY_COMMAND,
        ERROR_INCOMPLETE_INSTRUCTION,
        ERROR_INCORRECT_NUMBER_OF_PARAMS,
        ERROR_INVALID_COMMAND,
        ERROR_INVALID_PLACE_COMMAND_POSITION_VALUE,
        ERROR_INVALID_PLACE_COMMAND_FACE_PARAM_VALUE,
        ERROR_ROBOT_NOT_PLACED  
    } from '../src/constants';

describe('Testing RobotController: ', function() {
    const robo = new Robot();
    const robotController: RobotController = new RobotController(robo, new CommandSplitter(), new CommandValidator());
    
    describe("Error scenarios: ", function() {
        /*it("Test Invoke(): Throws an error when command is empty", function() {
            try {
                robotController.invoke("");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_EMPTY_COMMAND)
            }
        });*/
    
        it("Test Invoke(): Throws an error when an invalid command is passed", function() {
            try {
                robotController.invoke("Gibberish");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_INVALID_COMMAND)
            }
        });
    
        it("Test Invoke(): On PLACE command, throws an error when parameters for PLACE command is empty", function() {
            try {
                robotController.invoke("PLACE");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_INCOMPLETE_INSTRUCTION)
            }
        });
    
        it("Test Invoke(): On PLACE command, throws an error when all parameters for PLACE command are not passed ", function() {
            try {
                robotController.invoke("PLACE X,Y");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_INCORRECT_NUMBER_OF_PARAMS)
            }
        }); 

        it("Test Invoke(): On PLACE command, throws an error when X,Y parameters for PLACE command are not numbers ", function() {
            try {
                robotController.invoke("PLACE X,Y,FACE");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_INVALID_PLACE_COMMAND_POSITION_VALUE)
            }
        });
        
        it("Test Invoke(): On PLACE command, throws an error when FACE parameter for PLACE command is not valid ", function() {
            try {
                robotController.invoke("PLACE 1,2,FACE");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_INVALID_PLACE_COMMAND_FACE_PARAM_VALUE)
            }
        });

        it("Test Invoke(): MOVE command without first placing throws an error", function() {
            try {
                robotController.invoke("MOVE");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_ROBOT_NOT_PLACED);
            }
        }); 

        it("Test Invoke(): LEFT command without first placing throws an error", function() {
            try {
                robotController.invoke("LEFT");
            }
            catch(err) {
                expect(err.message).to.eq(ERROR_ROBOT_NOT_PLACED);
            }
        }); 
    });

    describe("Success scenarios: ", function() {
        beforeEach(function() {
            robo.place(0,0,"EAST");
        });

        describe("Testing commands ", function() {
            it("Test Invoke(): On PLACE command, returns correct robo position ", function() {
                expect(robotController.invoke("PLACE 0,1,EAST")).to.eq("0,1,EAST");            
            });
    
            it("Test Invoke(): On LEFT command, returns correct robo position ", function() {
                expect(robotController.invoke("LEFT")).to.eq("0,0,NORTH");            
            });

            it("Test Invoke(): On RIGHT command, returns correct robo position ", function() {
                expect(robotController.invoke("RIGHT")).to.eq("0,0,SOUTH");            
            });

            it("Test Invoke(): On MOVE command, returns correct robo position ", function() {
                expect(robotController.invoke("MOVE")).to.eq("1,0,EAST");            
            });

            it("Test Invoke(): On REPORT command, returns correct robo position ", function() {
                expect(robotController.invoke("REPORT")).to.eq("0,0,EAST");            
            });
        });   
        
        describe("Testing multiple instructions ", function() {
            it("Invoking REPORT command should return 3,3,NORTH", function() {
                robotController.invoke("PLACE 1,2,EAST");
                robotController.invoke("MOVE");
                robotController.invoke("MOVE");
                robotController.invoke("LEFT");
                robotController.invoke("MOVE");                

                expect(robotController.invoke("REPORT")).to.eq("3,3,NORTH");            
            }); 
            
            it("Invoking REPORT command should return 1,1,SOUTH", function() {
                robotController.invoke("PLACE 1,2,WEST");
                expect(robotController.invoke("RIGHT")).to.eq("1,2,NORTH");
                expect(robotController.invoke("MOVE")).to.eq("1,3,NORTH");

                expect(robotController.invoke("RIGHT")).to.eq("1,3,EAST");
                expect(robotController.invoke("MOVE")).to.eq("2,3,EAST");

                expect(robotController.invoke("RIGHT")).to.eq("2,3,SOUTH");
                expect(robotController.invoke("MOVE")).to.eq("2,2,SOUTH");

                expect(robotController.invoke("LEFT")).to.eq("2,2,EAST");
                expect(robotController.invoke("MOVE")).to.eq("3,2,EAST");

                expect(robotController.invoke("LEFT")).to.eq("3,2,NORTH");
                expect(robotController.invoke("MOVE")).to.eq("3,3,NORTH");

                expect(robotController.invoke("LEFT")).to.eq("3,3,WEST");
                robotController.invoke("MOVE");
                expect(robotController.invoke("MOVE")).to.eq("1,3,WEST");

                expect(robotController.invoke("LEFT")).to.eq("1,3,SOUTH");
                robotController.invoke("MOVE");
                expect(robotController.invoke("MOVE")).to.eq("1,1,SOUTH");                        
            }); 
        });   
    });
});

