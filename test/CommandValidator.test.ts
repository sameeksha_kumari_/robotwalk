import { CommandValidator } from '../src/CommandValidator';
import { expect } from 'chai';

describe('Testing CommandValidator', function() {
    const commandValidator: CommandValidator = new CommandValidator();
    it('Test isNotEmpty(): Returns true when command is not empty', function() {
        expect(commandValidator.isNotEmpty(["Command"])).to.eq(true);
    });

    it('Test isNotEmpty(): Returns true false command is empty', function() {
        expect(commandValidator.isNotEmpty([])).to.eq(false);
    });

    it("Test hasCorrectNumberOfParameters(): Returns true when has correct number of params", function() {
        expect(commandValidator.hasCorrectNumberOfParameters(["Command"], 1)).to.eq(true);
    });

    it("Test hasCorrectNumberOfParameters(): Returns false when does not have correct number of params", function() {
        expect(commandValidator.hasCorrectNumberOfParameters(["Command"], 2)).to.eq(false);
    });

    it("Test isNumber(): Returns true when number is passed", function() {
        expect(commandValidator.isNumber("1")).to.eq(true);
    });

    it("Test isNumber(): Returns false when number is passed", function() {
        expect(commandValidator.isNumber("garbage")).to.eq(false);
    });

    it("Test isValidValue(): Returns true when check value is found in valid values array", function() {
        expect(commandValidator.isValidValue(["Value1", "Value2"], "Value1")).to.eq(true);
    });

    it("Test isValidValue(): Returns false when check value is not found in valid values array", function() {
        expect(commandValidator.isValidValue(["Value1", "Value2"], "Value3")).to.eq(false);
    });
});