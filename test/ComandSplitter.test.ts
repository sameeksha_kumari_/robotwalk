import { CommandSplitter } from '../src/CommandSplitter';
import { expect } from 'chai';

describe('Testing CommandSplitter', function() {
    const commandSplitter: CommandSplitter = new CommandSplitter();
    it('Test split(): Behaves correctly when a command string devoid of split character is sent', function() {
        const splitResult = commandSplitter.split("CommandMock", ",");
        expect(splitResult.length).to.eq(1);
        expect(splitResult[0]).to.eq("CommandMock");
    });

    it('Test split()s: Behaves correctly when a command string consisting of split character is sent', function() {
        const splitResult = commandSplitter.split("CommandMock params", " ");
        expect(splitResult.length).to.eq(2);
        expect(splitResult[0]).to.eq("CommandMock");
        expect(splitResult[1]).to.eq("params");
    });
});