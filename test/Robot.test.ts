import { Board } from "../src/Robot";
import { Robot } from "../src/Robot";
import { expect } from "chai";
import { ERROR_ROBOT_NOT_PLACED, LEFT_DIRECTION, RIGHT_DIRECTION, ERROR_ROBOT_OUT_OF_BOARD } from '../src/constants';

describe("Testing Robot", function() {
    let robo: Robot = undefined;
    const defaultXPos = 0, defaultYPos = 0, defaultFace = "EAST";     
    
    describe("Testing controls", function() {
        beforeEach(function() {
            robo = new Robot();
            const xPos = defaultXPos, yPos = defaultYPos, face = defaultFace;
            robo.place(xPos, yPos, face);
        });
    
        it("Placing robot should report correctly", function() {
            const xPos = 3, yPos = 3, face = "EAST";
            robo.place(xPos, yPos, face);
            expect(robo.report()).to.eq(xPos+","+yPos+","+face);
        });
    
        it("Setting direction to left should work correctly", function() {
            const direction = LEFT_DIRECTION;
            robo.setDirection(direction);
            expect(robo.report()).to.eq(defaultXPos+","+defaultYPos+","+"NORTH");
        });    
    
        it("Setting direction to right should work correctly", function() {
            const direction = RIGHT_DIRECTION;
            robo.setDirection(direction);
            expect(robo.report()).to.eq(defaultXPos+","+defaultYPos+","+"SOUTH");
        });
        
        it("Move() works as expected", function() {
            robo.move();
            expect(robo.report()).to.eq("1,0,"+defaultFace);
        });
    });

    describe("Error scenarios: ", function() {
        beforeEach(function() {
            robo = new Robot();
        });

        it("Should throw an error when robo is moved without first placing", function() {
            try {
                robo.move();
            } catch(err) {
                expect(err.message).to.eq(ERROR_ROBOT_NOT_PLACED);
            }
        });

        it("Should throw an error when robo direction is set without first placing", function() {
            try {
                robo.setDirection(LEFT_DIRECTION);
            } catch(err) {
                expect(err.message).to.eq(ERROR_ROBOT_NOT_PLACED);
            }
        });

        it("Should throw an error when robo is placed off grid", function() {
            try {
                robo.place(5,5,"EAST");
            } catch(err) {
                expect(err.message).to.eq(ERROR_ROBOT_OUT_OF_BOARD);
            }
        });
    });

    describe("Testing movement in all directions", function() {
        beforeEach(function() {
            robo = new Robot();
        });

        it("Positioning robo on NORTH and moving should work correctly", function() {
            robo.place(2, 2, "NORTH");
            robo.move();
            expect(robo.report()).to.eq("2,3,NORTH");
        });
    
        it("Positioning robo on EAST and moving should work correctly", function() {
            robo.place(2, 2, "EAST");
            robo.move();
            expect(robo.report()).to.eq("3,2,EAST");
        });
    
        it("Positioning robo on WEST and moving should work correctly", function() {
            robo.place(2, 2, "WEST");
            robo.move();
            expect(robo.report()).to.eq("1,2,WEST");
        });
    
        it("Positioning robo on SOUTH and moving should work correctly", function() {
            robo.place(2, 2, "SOUTH");
            robo.move();
            expect(robo.report()).to.eq("2,1,SOUTH");
        });    
    });

    
    describe("Testing rotation back to origin", function() {
        beforeEach(function() {
            robo = new Robot();
            const xPos = defaultXPos, yPos = defaultYPos, face = defaultFace;
            robo.place(xPos, yPos, face);
        });
    
        it("Should return back to default face when rotated left 4 times ", function() {
            robo.setDirection(LEFT_DIRECTION);
            robo.setDirection(LEFT_DIRECTION);
            robo.setDirection(LEFT_DIRECTION);
            robo.setDirection(LEFT_DIRECTION);
    
            expect(robo.report()).to.eq(defaultXPos+","+defaultYPos+","+defaultFace);
        });

        it("Should return back to default face when rotated RIGHT 4 times ", function() {
 
            robo.setDirection(RIGHT_DIRECTION);
            robo.setDirection(RIGHT_DIRECTION);
            robo.setDirection(RIGHT_DIRECTION);
            robo.setDirection(RIGHT_DIRECTION);
    
            expect(robo.report()).to.eq(defaultXPos+","+defaultYPos+","+defaultFace);
        });
    
    });

    describe("Off the grid test ", function() {
        beforeEach(function() {
            robo = new Robot();
            const xPos = defaultXPos, yPos = defaultYPos, face = defaultFace;
            robo.place(xPos, yPos, face);
        });
    
        it("Move() * 6 times does not get robot off grid", function() {
            robo.move();
            robo.move();
            robo.move();
            robo.move();
            robo.move();
            robo.move();
    
            expect(robo.report()).to.eq("4,0,"+defaultFace);
        });
    
        it("Move() * 6 times along NORTH does not get robot off grid", function() {
            const xPos = 0, yPos = 0, face = "NORTH";
            robo.place(xPos, yPos, face);
    
            robo.move();
            robo.move();
            robo.move();
            robo.move();
            robo.move();
            robo.move();
    
            expect(robo.report()).to.eq("0,4,NORTH");
        });
    });    

    describe("Multiple instructions test ", function() {
        beforeEach(function() {
            robo = new Robot();
        });
        
        it("Should be placed at 3,3,NORTH", function() {
            robo.place(1, 2, "EAST");
            robo.move();
            robo.move();            
            robo.setDirection(LEFT_DIRECTION);
            robo.move();

            expect(robo.report()).to.eq("3,3,NORTH");
        });

        it("Should be placed at 0,1,WEST", function() {
            robo.place(1, 2, "SOUTH");
            robo.move();            
            robo.setDirection("RIGHT");
            robo.move();

            expect(robo.report()).to.eq("0,1,WEST");
        });
    });
});