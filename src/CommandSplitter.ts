export class CommandSplitter {
    split(command: string, splitByChar: string) {
        return command.split(splitByChar);
    }
}