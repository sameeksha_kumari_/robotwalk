import { RobotController } from './RobotController';
import { Robot } from './Robot';
import { CommandSplitter } from './CommandSplitter';
import { CommandValidator } from './CommandValidator';

let robotController: RobotController = new RobotController(new Robot(), new CommandSplitter(), new CommandValidator());

let standard_input = process.stdin;
standard_input.setEncoding('utf-8');

console.log("Enter command for robot:");
standard_input.on('data', function (input) {
    input = input.replace(/(\r\n|\n|\r)/gm, "");

    if(input === 'exit'){
        console.log("Bye Bye...");
        process.exit();
    } else
    {
        try {
            console.log("Robot report: " + robotController.invoke(input));
        } catch(err) {
            console.log(err.message);
        }        
    }

    console.log("\nEnter command for robot:");
});