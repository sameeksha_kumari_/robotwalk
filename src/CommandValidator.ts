export class CommandValidator {
    isNotEmpty(commandParameters: Array<string>) {
        return (commandParameters.length > 0) ? true : false;
    }

    hasCorrectNumberOfParameters(params: Array<string>, paramsCount: number) {
        return params.length === paramsCount;
    }  

    isNumber(param: string) {
        return (parseInt(param)).toString() !== "NaN";
    }

    isValidValue(validValues: Array<any>, checkValue: any) {
        return validValues.indexOf(checkValue) !== -1;
    }

    isInValidRange(maxValue: number, value: number) {
        return value <= maxValue;
    }
}