import { 
         ERROR_ROBOT_NOT_PLACED,
         ERROR_ROBOT_OUT_OF_BOARD,
         LEFT_DIRECTION,
         RIGHT_DIRECTION 
} from './constants';

export class Board {
    static xInit = 0;
    static yInit = 0;
    static xMax = 4;
    static yMax = 4;     
}

export class Robot {
    static zeroDeg = 0;
    static ninetyDeg = 90;
    static oneEightyDeg = 180;
    static twoSeventyDeg = 270;
    static moveDegrees = [Robot.zeroDeg, Robot.ninetyDeg, Robot.oneEightyDeg, Robot.twoSeventyDeg];

    static directionToDegree = {
        EAST: Robot.zeroDeg,
        NORTH: Robot.ninetyDeg,
        WEST: Robot.oneEightyDeg,
        SOUTH: Robot.twoSeventyDeg
    }

    static initialDirection = "WEST";

    static degreeToDirection = {
        "0": "EAST",
        "90": "NORTH",
        "180": "WEST",
        "270": "SOUTH"
    }

    private xPos;
    private yPos;
    private currDegree;   
    private currentFace; 
    private isPlaced;
   
    constructor() {
        this.xPos = 0;
        this.yPos = 0;
        this.currentFace = Robot.initialDirection;
        this.isPlaced = false;
    }

    report() {
        return this.xPos + "," + this.yPos + "," + this.currentFace;
    }

    place(x, y, face) {
        if(x > Board.xMax || y > Board.yMax) {
            throw new Error(ERROR_ROBOT_OUT_OF_BOARD);
        }

        this.xPos = x;
        this.yPos = y;
        this.currentFace = face;
        this.currDegree = Robot.directionToDegree[this.currentFace];
        if(this.isPlaced === false) this.isPlaced = true;
    }

    setDirection(direction) {
        if(!this.isPlaced) {
            throw new Error(ERROR_ROBOT_NOT_PLACED);
        }

        let degreePosition = Robot.moveDegrees.indexOf(this.currDegree);

        if(direction == LEFT_DIRECTION) {
            if(degreePosition === Robot.moveDegrees.length - 1) {
                degreePosition = 0;
            }
            else {
                degreePosition += 1;
            }
        }
    
        if(direction == RIGHT_DIRECTION) {
            if(degreePosition === 0) {
                degreePosition = Robot.moveDegrees.length - 1;
            }
            else {
                degreePosition -= 1;
            }
        }
    
        this.currDegree = Robot.moveDegrees[degreePosition];
        this.currentFace = Robot.degreeToDirection[this.currDegree];
    }

    move() {
        if(!this.isPlaced) {
            throw new Error(ERROR_ROBOT_NOT_PLACED);
        }

        switch(this.currDegree) {
            case Robot.zeroDeg: this.moveEast(); break;
            case Robot.ninetyDeg: this.moveNorth(); break;
            case Robot.oneEightyDeg: this.moveWest(); break;
            case Robot.twoSeventyDeg: this.moveSouth(); break;
        }
    }
    
    private moveEast() {
        if(this.xPos < Board.xMax) {
            this.xPos = this.xPos + 1;
        }
    }
    
    private moveWest() {
        if(this.xPos > Board.xInit) {
            this.xPos = this.xPos - 1;
        }
    }
    
    private moveNorth() {
        if(this.yPos < Board.yMax) {
            this.yPos = this.yPos + 1;
        }
    }
    
    private moveSouth() {
        if(this.yPos > Board.yInit) {
            this.yPos = this.yPos - 1;
        }
    }    
}