export const SPACE_SPLIT_CHAR = " ";
export const COMMA_PARAM_SPLIT_CHAR = ",";

export const ERROR_EMPTY_COMMAND = "Command cannot be empty";
export const ERROR_INCOMPLETE_INSTRUCTION = "Error: Instruction is not complete. ";
export const ERROR_INCORRECT_NUMBER_OF_PARAMS = "Error: Place does not have correct number of parameter. It requires X, Y, Face to be passed.";
export const ERROR_INVALID_COMMAND = "Error: Please input a valid command. Type 'HELP' to check all valid commands";
export const ERROR_INVALID_PLACE_COMMAND_POSITION_VALUE = "Error: X and Y position value for the place command should be a number";
export const ERROR_INVALID_PLACE_COMMAND_FACE_PARAM_VALUE = "Error: FACE parameter for this command can only have 'NORTH', 'EAST','WEST', 'SOUTH' as valid values";
export const ERROR_ROBOT_OUT_OF_BOARD = "Error: Robot cannot be placed out of the board.";

export const ERROR_ROBOT_NOT_PLACED = "Error: Robot should be placed first before any other instruction can take effect."

export const LEFT_DIRECTION = "LEFT";
export const RIGHT_DIRECTION = "RIGHT";

export const MAX_GRID_LENGTH = 4;
