import { Robot } from '../src/Robot';
import { CommandSplitter } from './CommandSplitter';
import { CommandValidator } from './CommandValidator';
import {    
            LEFT_DIRECTION,
            RIGHT_DIRECTION,
            MAX_GRID_LENGTH,
            SPACE_SPLIT_CHAR, 
            COMMA_PARAM_SPLIT_CHAR,
            ERROR_EMPTY_COMMAND,
            ERROR_INCOMPLETE_INSTRUCTION,
            ERROR_INCORRECT_NUMBER_OF_PARAMS,
            ERROR_INVALID_COMMAND,
            ERROR_INVALID_PLACE_COMMAND_POSITION_VALUE,
            ERROR_INVALID_PLACE_COMMAND_FACE_PARAM_VALUE
        } from './constants';

export class RobotController {
    robo: Robot;
    commandSplitter: CommandSplitter;
    commandValidator: CommandValidator;

    constructor(robo: Robot, commandSplitter: CommandSplitter, commandValidator: CommandValidator) {
        this.robo = robo;
        this.commandSplitter = commandSplitter;
        this.commandValidator = commandValidator;
    }

    invoke(command: string) {   
        try {
            const commandParams: Array<string> = this.commandSplitter.split(command, SPACE_SPLIT_CHAR);
            if(this.commandValidator.isNotEmpty(commandParams) === false) {
                throw new Error(ERROR_EMPTY_COMMAND);
            };

            const commandIdentifier: string = commandParams[0];
            switch(commandIdentifier) {
                case "PLACE": this.place(commandParams); break;
                case "MOVE": this.move(); break;
                case "LEFT": this.setDirection(LEFT_DIRECTION); break;
                case "RIGHT": this.setDirection(RIGHT_DIRECTION); break;
                case "REPORT": break;  // report is sent for every command
                case "HELP": return this.help(); 
                default: this.invalidCommand(); break;
            }

            return this.robo.report();
        }
        catch(err) {
            throw err;
        }         
    }    

    private place(placeInstruction: Array<string>) {
        if(this.commandValidator.hasCorrectNumberOfParameters(placeInstruction, 2) == false) {
            throw new Error(ERROR_INCOMPLETE_INSTRUCTION)
        }
        const placeParams = placeInstruction[1].split(COMMA_PARAM_SPLIT_CHAR);
        
        if(this.commandValidator.hasCorrectNumberOfParameters(placeParams, 3) === false) {
            throw new Error(ERROR_INCORRECT_NUMBER_OF_PARAMS)
        }
      
        if(this.commandValidator.isNumber(placeParams[0]) === false
           || this.commandValidator.isNumber(placeParams[1]) === false
        ) {
            throw new Error(ERROR_INVALID_PLACE_COMMAND_POSITION_VALUE);
        }        

        if(this.commandValidator.isValidValue(["NORTH", "EAST", "WEST", "SOUTH"], placeParams[2]) === false) {
            throw new Error(ERROR_INVALID_PLACE_COMMAND_FACE_PARAM_VALUE);
        }

        this.robo.place(parseInt(placeParams[0]), parseInt(placeParams[1]), placeParams[2]);
    }

    private move() {
        this.robo.move();
    }
    
    private setDirection(direction: string) {
        this.robo.setDirection(direction);
    }

    private help() {
        return "Valid Robot Commands: PLACE X,Y,F | MOVE | LEFT | RIGHT | REPORT";
    }

    private invalidCommand() {
        throw new Error(ERROR_INVALID_COMMAND);
    }
}